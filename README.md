# Overview
Open a video: https://gitlab.com/v4scale_public/demo_logs/-/raw/main/video.mp4

## For run:  

### 1. Copy logs file to `./logs`
### 2. Build image
```
$ docker-compose build
```

### 3. Run:
```
$ docker-compose up
```

### Open in browser: 
http://localhost:3000/

### To restart after first start:
```
$ docker-compose down --volumes && docker-compose build && docker-compose up
```
