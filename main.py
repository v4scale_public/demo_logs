import glob
import time
from datetime import datetime
import os
import re
import psycopg2

db_host = "localhost"
if 'DB_HOST' in os.environ:
    db_host = os.environ["DB_HOST"]


class TestFile:
    filename: str
    test_name: str
    start_time: datetime
    test_time: float = None
    HB_SN: str
    ONIX_SN: str
    log: str
    result: str

    def __init__(self, filename: str):
        self.filename: str = filename

        self.parse_file()

    def parse_file(self):
        file_content: str
        with open(self.filename, 'r') as fd:
            file_content = fd.read()

        tests = [i.strip() for i in file_content.split("==================================================")]
        tests = [x for x in tests if x != '']

        if len(tests) != 1:
            raise ValueError('Not one test on file ' + self.filename)

        self.parse_test(tests[0])

    def parse_test(self, text: str):
        [header, self.log, result] = [i.strip() for i in
                                      text.split("--------------------------------------------------")]
        [time, meta_str] = [i.strip() for i in header.split("\n")]
        self.start_time = datetime.strptime(time, '%Y-%m-%d-%H:%M:%S')

        [HB_SN_str, ONIX_SN_str] = [i.strip() for i in meta_str.split(", ")]
        self.HB_SN = [i.strip() for i in HB_SN_str.split(":")][1]
        self.ONIX_SN = [i.strip() for i in ONIX_SN_str.split(":")][1]

        [self.test_name, self.result] = [i.strip() for i in result.split(":")]

        test_time_lines = [line for line in self.log.split('\n') if "Test completed in" in line]

        if len(test_time_lines) > 0:
            test_time_str = re.findall("\d+\.\d+", test_time_lines[0])[0]
            self.test_time = float(test_time_str)


log_list: list[TestFile] = []
for f in glob.glob("./logs/*"):
    log_list.append(TestFile(f))

print(f"Parsed {len(log_list)} files with logs")

scheme_db = '''
create table if not exists tests
(
	id         serial not null,
	test_name  text,
	start_time timestamp,
	test_time  real,
	hb_sn      text,
	onix_sn    text,
	log        text,
	result     text
);
'''

print("Wait DB up...")
time.sleep(5)
print("Write to DB")

with psycopg2.connect(dbname='postgres', user='postgres', password='example', host=db_host) as conn:
    with conn.cursor() as cursor:
        print("Create a table")
        cursor.execute(scheme_db)
        print("Save test result to DB")

        for t in log_list:
            cursor.execute(
                '''INSERT INTO tests (test_name, start_time, test_time, hb_sn, onix_sn, log, result) VALUES (
                    %(test_name)s,
                    %(start_time)s,
                    %(test_time)s,
                    %(hb_sn)s,
                    %(onix_sn)s,
                    %(log)s,
                    %(result)s
                )''',
                {
                    'test_name': t.test_name,
                    'start_time': t.start_time,
                    'test_time': t.test_time,
                    'hb_sn': t.HB_SN,
                    'onix_sn': t.ONIX_SN,
                    'log': t.log,
                    'result': t.result,
                }
            )

print("Done")
